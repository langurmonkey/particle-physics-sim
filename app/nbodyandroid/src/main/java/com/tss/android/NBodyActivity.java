package com.tss.android;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.PorterDuff.Mode;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;

import com.lamerman.FileDialog;
import com.lamerman.SelectionMode;
import com.nbody.core.game.NBodyGame;
import com.nbody.core.graph.GameRenderer;
import com.nbody.core.graph.GridHandler;
import com.nbody.core.util.Constants;
import com.nbody.core.util.Keys;
import com.nbody.core.xml.DomParser;
import com.nbody.core.xml.SimulationBean;
import com.tss.android.prefs.CustomizeDialog;

import java.io.File;

public class NBodyActivity extends Activity {
    public static final String PREFS_NAME = "NBodyPrefsFile";
    static final int REQUEST_SAVE = 1;
    static final int REQUEST_LOAD = 2;
    static final int DIALOG_ABOUT = 0;
    public static String versionName;
    /**
     * Flag to check if we're back from settings and must restart
     */
    private static boolean backFromSettings = false;
    private static boolean backFromLoading = false;
    /**
     * A handle to the thread that's actually running the animation.
     */
    private GameThread gameThread;
    private GLSurfaceView mGLSurfaceView;
    private GameRenderer mGameRenderer;
    private PowerManager.WakeLock wl;
    private TextView mFpsText, mNcountText;
    private SeekBar mVelocitySeekBar;
    private boolean infoVisible = false;
    private SimulationBean sb;
    private Handler mHandler = new Handler();
    private Runnable mUpdateTextView = new Runnable() {
        public void run() {
            if (NBodyGame.singleton() != null) {
                int n = NBodyGame.singleton().bh ? NBodyGame.singleton().bodies.mCount - 1
                        : NBodyGame.singleton().bodies.mCount;
                mNcountText.setText(n + " particles");
            }
            if (mGameRenderer != null) {
                Float fps = mGameRenderer.fps;
                fps = fps - (fps * 100f - (int) (fps * 100f)) / 100f;
                mFpsText.setText(fps.toString() + "fps");
            }

            if (infoVisible)
                mHandler.postDelayed(mUpdateTextView, 1000);
        }
    };

    /**
     * Invoked when the Activity is created.
     *
     * @param savedInstanceState a Bundle containing state saved from a previous
     *                           execution, or null if this is a new execution
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            versionName = this.getPackageManager().getPackageInfo(this.getPackageName(), 0).versionName;
        } catch (NameNotFoundException e1) {
            e1.printStackTrace();
        }

        GridHandler.useHardwareBuffers = true;
        GridHandler.useVerts = true;

        // Turn off the window's title bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        // Tell system to use the layout defined in our XML file
        // setContentView(R.layout.main);
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        // Acquire sleep lock
        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        wl = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "pps:mywakelogtag");
        wl.acquire();

        // Create the game thread
        gameThread = new GameThread(getApplicationContext(), new Handler() {
            @Override
            public void handleMessage(Message m) {
            }
        }, this);
        gameThread.setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            public void uncaughtException(Thread t, Throwable e) {
                Log.e("NBody", t + " threw exception: " + e);
            }
        });

        // Create surface view
        setContentView(R.layout.main);
        mGLSurfaceView = findViewById(R.id.glsurfaceview);
        mGLSurfaceView.setFocusable(true);
        mGLSurfaceView.setOnTouchListener(gameThread);
        gameThread.setSurfaceSize(dm.widthPixels, dm.heightPixels);
        gameThread.setRunning(true);
        gameThread.doStart();
        gameThread.start();

        // INITIALIZE BUTTONS
        int buttonSize = 25;

        // Viewport manipulation
        Interface.mPanButton = findViewById(R.id.panButton);
        Interface.mPanButton.setOnTouchListener(gameThread);
        setButtonSize(Interface.mPanButton, buttonSize);

        Interface.mResetCameraButton = findViewById(R.id.resetCameraButton);
        Interface.mResetCameraButton.setOnTouchListener(gameThread);
        setButtonSize(Interface.mResetCameraButton, buttonSize);

        // Simulation interaction
        Interface.mShootButton = findViewById(R.id.shootButton);
        Interface.mShootButton.setOnTouchListener(gameThread);
        setButtonSize(Interface.mShootButton, buttonSize);

        Interface.mRepelButton = findViewById(R.id.repelButton);
        Interface.mRepelButton.setOnTouchListener(gameThread);
        setButtonSize(Interface.mRepelButton, buttonSize);

        Interface.mWallButton = findViewById(R.id.wallButton);
        Interface.mWallButton.setOnTouchListener(gameThread);
        setButtonSize(Interface.mWallButton, buttonSize);

        Interface.mAccelButton = findViewById(R.id.accelButton);
        Interface.mAccelButton.setOnTouchListener(gameThread);
        setButtonSize(Interface.mAccelButton, buttonSize);

        Interface.mTailButton = findViewById(R.id.tailButton);
        Interface.mTailButton.setOnTouchListener(gameThread);
        Interface.mTailButton.setColorFilter(Interface.buttonColor, Mode.MULTIPLY);
        setButtonSize(Interface.mTailButton, buttonSize);

        Interface.mFpsButton = findViewById(R.id.fpsButton);
        Interface.mFpsButton.setOnTouchListener(gameThread);
        setButtonSize(Interface.mTailButton, buttonSize);

        Interface.mMenuButton = findViewById(R.id.menuButton);
        Interface.mMenuButton.setOnTouchListener((view, event) -> {
            NBodyActivity.this.openOptionsMenu();
            return true;
        });

        mFpsText = findViewById(R.id.fps);
        mNcountText = findViewById(R.id.ncount);

        mVelocitySeekBar = findViewById(R.id.velocitySeekbar);
        mVelocitySeekBar.setMax(100);
        mVelocitySeekBar.setOnSeekBarChangeListener(gameThread);
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());
        mVelocitySeekBar.setProgress(sp.getInt(Keys.KEY_VELOCITY, Constants.DEFAULT_VELOCITY));

        mGameRenderer = new GameRenderer(getApplicationContext(), dm.widthPixels, dm.heightPixels);
        mGameRenderer.setVertMode(GridHandler.useVerts, GridHandler.useHardwareBuffers);
        mGLSurfaceView.setRenderer(mGameRenderer);

        gameThread.setmRenderer(mGameRenderer);

    }

    private void setButtonSize(ImageButton ib, int size) {
        ib.setMaxHeight(size);
        ib.setMaxWidth(size);
    }

    /**
     * Invoked during init to give the Activity a chance to set up its Menu.
     *
     * @param menu the Menu to which entries may be added
     * @return true
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.game_menu, menu);
        return true;
    }

    /**
     * Invoked when the user selects an item from the Menu.
     *
     * @param item the Menu entry which was selected
     * @return true if the Menu item was legit (and we consumed it), false
     * otherwise
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.item_restart:
                mGLSurfaceView.onPause();
                gameThread.pause();
                gameThread.reload();
                gameThread.unpause();
                mGLSurfaceView.onResume();
                return true;
            case R.id.item_settings:
                mGLSurfaceView.onPause();
                gameThread.pause();
                backFromSettings = true;
                Intent prefs = new Intent(getApplicationContext(), SetPrefs.class);
                startActivityForResult(prefs, 0);
                return true;
            case R.id.item_about:
                mGLSurfaceView.onPause();
                gameThread.pause();
                CustomizeDialog customizeDialog = new CustomizeDialog(this, mGLSurfaceView, gameThread);
                customizeDialog.setOwnerActivity(this);
                customizeDialog.show();
                return true;
            case R.id.item_loadxml:
                mGLSurfaceView.onPause();
                gameThread.pause();

                intent = new Intent(getBaseContext(), FileDialog.class);
                intent.putExtra(FileDialog.START_PATH, "/sdcard");

                // can user select directories or not
                intent.putExtra(FileDialog.CAN_SELECT_DIR, false);
                intent.putExtra(FileDialog.SELECTION_MODE, SelectionMode.MODE_OPEN);

                // alternatively you can set file filter
                intent.putExtra(FileDialog.FORMAT_FILTER, new String[]{"xml"});

                startActivityForResult(intent, REQUEST_LOAD);

                return true;
            case R.id.item_savexml:
                mGLSurfaceView.onPause();
                gameThread.pause();

                intent = new Intent(getBaseContext(), FileDialog.class);
                intent.putExtra(FileDialog.START_PATH, "/sdcard");

                // can user select directories or not
                intent.putExtra(FileDialog.CAN_SELECT_DIR, false);
                intent.putExtra(FileDialog.SELECTION_MODE, SelectionMode.MODE_CREATE);

                // alternatively you can set file filter
                intent.putExtra(FileDialog.FORMAT_FILTER, new String[]{"xml"});

                startActivityForResult(intent, REQUEST_SAVE);

                return true;
        }

        return false;
    }

    public synchronized void onActivityResult(final int requestCode, int resultCode, final Intent data) {

        if (resultCode == Activity.RESULT_OK) {
            String filePath = data.getStringExtra(FileDialog.RESULT_PATH);
            if (requestCode == REQUEST_SAVE) {
                Log.i(this.getClass().getName(), "Saving...");
                SimulationBean sb = gameThread.getSimulationBean();
                String fileName = filePath + (filePath.endsWith(".xml") ? "" : ".xml");
                File f = new File(fileName);
                // Store sb in file
                DomParser parser = new DomParser();
                boolean ok = parser.writeXml(sb, f);

                showAlertDialog(ok ? "Simulation saved to " + f.getName()
                        : "Simulation not saved (file already exists?)");

            } else if (requestCode == REQUEST_LOAD) {
                Log.i(this.getClass().getName(), "Loading...");

                // Load sb from file
                DomParser parser = new DomParser();
                File f = new File(filePath);
                sb = parser.parseXml(f);
                if (sb != null) {
                    backFromLoading = true;
                    showAlertDialog("Simulation loaded successfully from " + f.getName());
                } else {
                    showAlertDialog("Loading of " + f.getName() + " failed");
                }

            }

        } else if (resultCode == Activity.RESULT_CANCELED) {
            Log.w(this.getClass().getName(), "file not selected");
        } else {
            Log.e(this.getClass().getName(), "Ooops, what did just happen?");
        }

    }

    /**
     * Invoked when the Activity loses user focus.
     */
    @Override
    protected void onPause() {
        super.onPause();
        gameThread.pause();
        mGLSurfaceView.onPause();
    }

    /**
     * Notification that something is about to happen, to give the Activity a
     * chance to save state.
     *
     * @param outState a Bundle into which this Activity should save its state
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        // just have the View's thread save its state into our Bundle
        super.onSaveInstanceState(outState);
        gameThread.saveState(outState);
        Log.w(this.getClass().getName(), "SIS called");
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (backFromSettings && SetPrefs.changes > 0) {
            Log.i(this.getClass().getName(), "BACK FROM SETTINGS!");
            // reload
            gameThread.reload();
            gameThread.unpause();
            mGLSurfaceView.onResume();
        } else if (backFromLoading) {
            Log.i(this.getClass().getName(), "BACK FROM LOADING!");
            updateControlsFromBean(sb);
            gameThread.reload(sb);
            gameThread.unpause();
            mGLSurfaceView.onResume();
        } else {
            gameThread.unpause();
            mGLSurfaceView.onResume();
        }
        backFromLoading = false;
        backFromSettings = false;

        // Button visibility
        if (NBodyGame.singleton().simAreaScreen) {
            // Remove pan and center buttons
            Interface.mPanButton.setVisibility(View.INVISIBLE);
            Interface.mResetCameraButton.setVisibility(View.INVISIBLE);
        } else {
            Interface.mPanButton.setVisibility(View.VISIBLE);
            Interface.mResetCameraButton.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Updates the controls state using the given bean
     *
     * @param sb
     */
    private void updateControlsFromBean(SimulationBean sb) {
        // Update seekbar position
        mVelocitySeekBar.setProgress(NBodyGame.decodeDtscale(sb.dtscale));

        // Update accelerometer control
        if (sb.accelerometer)
            Interface.mAccelButton.setColorFilter(Interface.buttonColor, Mode.MULTIPLY);
        else
            Interface.mAccelButton.clearColorFilter();

        // Update tail control
        if (sb.showTail)
            Interface.mTailButton.setColorFilter(Interface.buttonColor, Mode.MULTIPLY);
        else
            Interface.mTailButton.clearColorFilter();
    }

    private void initThread(Bundle savedInstanceState) {

        if (savedInstanceState == null) {
            // we were just launched: set up a new game
            gameThread.setState(GameThread.STATE_RUNNING);
            Log.w(this.getClass().getName(), "SIS is null");
        } else {
            // we are being restored: resume a previous game
            gameThread.restoreState(savedInstanceState);
            Log.w(this.getClass().getName(), "SIS is nonnull");
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        boolean retry = true;
        gameThread.setRunning(false);
        while (retry) {
            try {
                gameThread.join();
                retry = false;
            } catch (InterruptedException e) {
            }
        }
        gameThread = null;
        wl.release();
    }

    public boolean isInfoVisible() {
        return infoVisible;
    }

    public void setInfoVisibility(boolean visible) {
        setVisible(mFpsText, visible);
        setVisible(mNcountText, visible);
        infoVisible = visible;

        // Set visibility of fps and ncount
        mGameRenderer.showFps = visible;
        if (visible)
            mHandler.post(mUpdateTextView);
    }

    private void setVisible(View v, boolean visible) {
        if (visible)
            v.setVisibility(View.VISIBLE);
        else
            v.setVisibility(View.INVISIBLE);
    }

    private void setText(TextView v, String text) {
        if (v != null)
            v.setText(text);
    }

    private void showAlertDialog(String text) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(text).setCancelable(false).setPositiveButton("Cool!", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        }).create();
        builder.show();
    }
}
