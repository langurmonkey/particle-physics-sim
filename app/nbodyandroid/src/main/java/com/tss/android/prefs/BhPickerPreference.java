package com.tss.android.prefs;

import android.content.Context;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.tss.android.R;

public class BhPickerPreference extends DialogPreference {
    private static final String androidns = "http://schemas.android.com/apk/res/android";

    private Context mContext;
    private int mValue, mDefault = 0;

    private RadioGroup radioGroup;
    private RadioButton radio1, radio2, radio3;
    private int id1, id2, id3;

    public BhPickerPreference(Context context, AttributeSet attrs) {
	super(context, attrs);
	mContext = context;

	mDefault = attrs.getAttributeIntValue(androidns, "defaultValue", 0);
    }

    @Override
    protected View onCreateDialogView() {
	LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	View v = inflater.inflate(R.layout.bhpicker, null);

	radioGroup = v.findViewById(R.id.bhPicker);
	radioGroup.setSaveEnabled(true);

	radio1 = v.findViewById(R.id.radioButton1);
	radio2 = v.findViewById(R.id.radioButton2);
	radio3 = v.findViewById(R.id.radioButton3);

	id1 = radio1.getId();
	id2 = radio2.getId();
	id3 = radio3.getId();

	onSetInitialValue(true, mDefault);

	return v;

    }

    protected void onSetInitialValue(boolean restore, int defaultValue) {
	super.onSetInitialValue(restore, defaultValue);
	if (restore)
	    mValue = shouldPersist() ? getPersistedInt(defaultValue) : defaultValue;
	else
	    mValue = defaultValue;

	switch (mValue) {
	case 0:
	    radioGroup.check(id1);
	    break;
	case 1:
	    radioGroup.check(id2);
	    break;
	case 2:
	    radioGroup.check(id3);
	    break;
	}
    }

    @Override
    protected void onDialogClosed(boolean positiveResult) {
	super.onDialogClosed(positiveResult);
	if (positiveResult) {
	    int id = radioGroup.getCheckedRadioButtonId();
	    if (id == id1)
		mValue = 0;
	    if (id == id2)
		mValue = 1;
	    if (id == id3)
		mValue = 2;

	    if (shouldPersist())
		persistInt(mValue);
	    callChangeListener(new Integer(mValue));
	}
    }

}