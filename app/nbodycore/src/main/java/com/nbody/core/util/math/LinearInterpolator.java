package com.nbody.core.util.math;

/**
 * A very simple linear interpolator.
 * 
 * @author Toni Sagrista
 * 
 */
public class LinearInterpolator {
	private float[] x;
	private float[] y;

	public LinearInterpolator(float[] x, float[] y) {
		assert x.length == y.length : "Both vectors must be the same size";
		assert x.length > 1 : "Vector size must be at least 2";
		assert x[0] < x[1] : "Vector x must be in ascending order (" + x[0] + " < " + x[1] + " ?";
		this.x = x;
		this.y = y;
	}

	public float value(float xv) {
		assert x[0] <= xv && x[x.length - 1] >= xv : "Given value must be in range";
		int idx = 0;
		for (int i = 0; i < x.length; i++) {
			if (xv == x[i])
				return y[i];
			if (xv > x[i]) {
				idx = i;
				break;
			}
		}
		return y[idx] + (xv - x[idx]) * (y[idx + 1] - y[idx]) / (x[idx + 1] - x[idx]);

	}

	public int getLowerIndex(float xv) {
		int idx = 0;
		for (int i = 0; i < x.length; i++) {
			if (xv == x[i])
				return i;
			if (xv > x[i]) {
				idx = i;
				break;
			}
		}
		return idx;
	}
}
