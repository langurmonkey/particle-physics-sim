package com.nbody.core.util;

import java.util.Random;

/**
 * Constants class
 * 
 * @author Toni Sagrista
 * 
 */
public class Constants {

	public static final int SCALE_STANDARD_H = 800;

	// Log constants
	public static final String LOG_TOUCH = "Touch";

	public static final String SIMAREA_SCREEN = "screen";
	public static final String SIMAREA_BIG = "big";

	public static final int DEFAULT_VELOCITY = 15;

	/**
	 * Universal gravitational constant
	 */
	public static final float G = 6.67428E-11f; // G - [m^3 Kg^-1 s^-2]
	/**
	 * Soft length to prevent singularity
	 */
	public static final float eps = 8;
	public static final float eps2 = (float) Math.pow(eps, 2);
	// Repulsion force mass
	public static final float RFmass = 9E11f;
	public static final float RFmassTimesG = G * RFmass;

	public static final Random rng = new Random();

	// preference names
	public static final String PREF_BOUNDARY = "boundaryConditions";
	public static final String PREF_COLLISIONS = "collisionPolicy";
	public static final String PREF_BGCOLOR = "bgColor";
	public static final String PREF_SHOWSPARKS = "showSparks";
	public static final String PREF_RESTOREPARTICLES = "restoreParticles";
	public static final String PREF_BGIMAGE = "bgImage";
	public static final String PREF_FORCEMETHOD = "forceCalculator";
	public static final String PREF_PARTICLECOUNT = "particleCount";
	public static final String PREF_PARTICLECOLOR = "color";
	public static final String PREF_SHOWTRAIL = "showTrail";
	public static final String PREF_GSTRENGTH = "gStrength";
	public static final String PREF_ACCELEROMETER = "accelerometer";
	public static final String PREF_SIMAREA = "simulationArea";
	public static final String PREF_CHOOSEBH = "chooseBh";
	public static final String PREF_CHOOSESTAR = "chooseStar";
	public static final String PREF_DISPLAYGRID = "displayGrid";
	public static final String PREF_DISPLAYMESHPOINTS = "displayMeshPoints";
	public static final String PREF_MESHDENSITY = "meshDensity";
	public static final String PREF_TAILLENGTH = "tailLength";
	public static final String PREF_PARTICLEMASS = "particleMass";
	public static final String PREF_FRICTION = "friction";
}
