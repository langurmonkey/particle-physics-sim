package com.nbody.core.graph;

import java.util.HashMap;
import java.util.Map;

import javax.microedition.khronos.opengles.GL10;

import android.content.Context;

import com.nbody.core.R;

/**
 * A texture handler which pre-loads textures and serves them to draw.
 * 
 * @author Toni Sagrista
 * 
 */
public class TextureHandler {

	public static Map<Integer, Texture> textures = new HashMap<Integer, Texture>();

	public static void loadTexture(Context context, GL10 gl, String path, int id) {
		if (!textures.containsKey(id)) {
			textures.put(id, new Texture(path).loadTexture(context, gl));
		} else {
			Texture t = textures.get(id);
			t.mPath = path;
			t.loadTexture(context, gl);
			textures.put(id, t);
		}
	}

	public static void loadTexture(Context context, GL10 gl, int resourceId) {
		if (!textures.containsKey(resourceId))
			textures.put(resourceId, new Texture(resourceId).loadTexture(context, gl));
	}

	public static void allocateTextures(int... resourceIds) {
		for (int resourceId : resourceIds)
			allocateTexture(resourceId);
	}

	public static void allocateTexture(int resourceId, boolean fromResource) {
		if (!textures.containsKey(resourceId))
			textures.put(resourceId, new Texture(resourceId, fromResource));
	}

	public static void allocateTexture(int resourceId) {
		if (!textures.containsKey(resourceId))
			textures.put(resourceId, new Texture(resourceId));
	}

	public static void loadAll(Context context, GL10 gl) {
		for (Integer key : textures.keySet()) {
			Texture t = textures.get(key);
			if (t.mFromResource)
				t.loadTexture(context, gl);
		}
	}

	public static void unloadAll(Context context, GL10 gl) {
		for (Integer key : textures.keySet()) {
			textures.get(key).unloadTexture(context, gl);
		}
		textures.clear();

	}

	public static Texture getTexture(Integer resourceId) {
		return textures.get(resourceId);
	}

	public static Texture getStarTexture(int id) {
		switch (id) {
			case 0:
				return getTexture(R.drawable.star);
			case 1:
				return getTexture(R.drawable.star2);
			case 2:
				return getTexture(R.drawable.star3);
			default:
				return getTexture(R.drawable.star);
		}
	}

	public static Texture getAntiStarTexture(int id) {
		switch (id) {
			case 0:
				return getTexture(R.drawable.antistar);
			case 1:
				return getTexture(R.drawable.antistar2);
			case 2:
				return getTexture(R.drawable.antistar3);
			default:
				return getTexture(R.drawable.antistar);
		}
	}

	public static Texture getStaticStarTexture(int id) {
		switch (id) {
			case 0:
				return getTexture(R.drawable.staticstar);
			case 1:
				return getTexture(R.drawable.staticstar2);
			case 2:
				return getTexture(R.drawable.staticstar3);
			default:
				return getTexture(R.drawable.staticstar);
		}
	}

	public static Texture getBhTexture() {
		return getTexture(R.drawable.bh);
	}

	public static Texture getBhTexture(int id) {
		switch (id) {
			case 0:
				return getTexture(R.drawable.bh);
			case 1:
				return getTexture(R.drawable.bh2);
			default:
				return getTexture(R.drawable.bh);
		}
	}
}
