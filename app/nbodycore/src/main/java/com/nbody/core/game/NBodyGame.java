package com.nbody.core.game;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.preference.PreferenceManager;
import android.util.FloatMath;
import android.util.Log;
import android.view.MotionEvent;

import com.nbody.core.R;
import com.nbody.core.game.Body.StarType;
import com.nbody.core.geom.Rectangle;
import com.nbody.core.geom.Vector2;
import com.nbody.core.graph.TextureHandler;
import com.nbody.core.util.Constants;
import com.nbody.core.util.FixedSizeArray;
import com.nbody.core.util.Keys;
import com.nbody.core.util.Locks;
import com.nbody.core.util.PolygonPool;
import com.nbody.core.util.VectorPool;
import com.nbody.core.util.math.LinearInterpolator;
import com.nbody.core.xml.ParticleBean;
import com.nbody.core.xml.SimulationBean;
import com.nbody.core.xml.WallBean;

/**
 * The main class of the simulation, initializes and handles the simulation context data.
 *
 * @author Toni Sagrista
 *
 */
public class NBodyGame {

	public enum TouchState {
		PAN, SHOOT, SHOOT_ANTI, SHOOT_STATIC, REPEL, WALL, NONE
	}

	public enum Event {
		SHOOT, REPEL, WALL, PAN, ZOOM, NONE
	}

	public enum CollisionPolicy {
		ELASTIC, MERGERS, NONE
	}

	public enum ForceMethod {
		P3M, DIRECT, PM
	}

	public enum Friction {
		NO, NONE, SMALL, MEDIUM, LARGE, EXTRALARGE
	}

	float oldDist = 0f;

	// Singleton pattern
	public static NBodyGame singleton() {
		return singleton;
	}

	public static NBodyGame initSingleton(NBodyGame nbodygame) {
		singleton = nbodygame;
		return singleton;
	}

	private static NBodyGame singleton;
	public boolean initialized = false;

	public static final float rotForceModulus = .45f;

	private static final float vmax = 300;

	/**
	 * The width
	 */
	public static int w;

	/**
	 * The height
	 */
	public static int h;
	public static int ntail = 60;
	public boolean simAreaScreen = false;
	public static final int boundaryBigSide = 3000;
	public static int boundaryW = boundaryBigSide; // Number of pixels the
	// boundary extends off the
	// screen
	public static int boundaryH = boundaryBigSide; // Number of pixels the
	// boundary extends off the
	// screen

	public static final int maxWalls = 50;
	public static final int maxSparks = 40;
	public static final int maxBodies = 800;

	public static float zoom;
	public static final float maxScale = 3f;
	public static final float minScale = 0.2f;

	public static float bgRed = 0f;
	public static float bgGreen = 0f;
	public static float bgBlue = 0f;
	public static float bgAlpha = 1f;

	public static String bgImage = null;

	public static Friction friction = Friction.NONE;
	public static float frictionScalar = 0f;

	/**
	 * Event controls
	 */
	public static Event currentEvent = Event.NONE;

	/**
	 * Sensors
	 */
	public static boolean accelerometer;
	private static float gx = 0;
	private static float gy = 0;

	/**
	 * Controls speed of simulation. Low value - speed up High value - slow down
	 */
	public float dtscale;

	/**
	 * How gravity decreases with distance
	 */
	public static double gstrength;
	/**
	 * Boundary rectangle
	 */
	public static Rectangle boundary;
	public float panBoundX, panBoundY;

	/**
	 * FLAGS
	 */
	/**
	 * True to apply boundary conditions
	 */
	private boolean bc = true;
	/**
	 * Use black hole
	 */
	public boolean bh = true;
	/**
	 * False for collisionless system
	 */
	public static CollisionPolicy collisions = CollisionPolicy.NONE;
	/**
	 * Force calculation method
	 */
	public static ForceMethod forceMethod = ForceMethod.PM;
	/**
	 * True to show sparks when stars collide
	 */
	public static boolean showSparks = true;
	/**
	 * True if sound enabled
	 */
	public static boolean sound = false;
	/**
	 * Show particle trail
	 */
	public static boolean showTrail = true;
	/**
	 * Restore particles when they go away
	 */
	private static boolean restore = true;
	/**
	 * Display mesh densities in PM method
	 */
	public static boolean displayGrid = true;

	/**
	 * Display mesh points
	 */
	public static boolean displayMeshPoints = true;
	/**
	 * Mesh density
	 */
	public static int meshDensity = 5;

	/**
	 * GLOBAL VALUES
	 */
	/**
	 * Number of particles
	 */
	public int N = 0;

	/**
	 * Galaxy radius
	 */
	public float r;

	/**
	 * Initial particle mass
	 */
	public static float particleMass = Body.minMass;

	/**
	 * Maximum velocity
	 */
	public float maxVel = 3f;

	/**
	 * List of bodies
	 */
	public FixedSizeArray<Body> bodies;
	public FixedSizeArray<Sparks> sparks;
	public FixedSizeArray<Wall> walls;
	public EventMotion charge;
	public EventMotion wall;
	public static Camera camera;
	public static Background background = null;

	private SimulationBean sb = null;

	// Bh sprite
	// 0 - concentric circles
	// 1 - Image sprite
	// 2 - No black hole
	public static int bhSprite = 0;

	// Star sprite
	// 0 - legacy
	// 1 - fade
	public static int starSprite = 0;

	public Body blackHole;

	/**
	 * GLOBAL VECTORS repPos - Acts as initial shooting point and repulsion
	 * point centre - Screen central point fvec - Used in force calc routine,
	 * has vector uniting two particles un - Used to compute elastic collisions
	 * ut - Used to compute elastic collisions aux - Used as auxiliar vector in
	 * elastic collisions
	 */
	public static Vector2 repPos, centre, fvec;
	public static Vector2 un, ut, aux;

	// Pointer up flag, which marks the first pointer has been lifted in a
	// multitouch event. Intended to prevent single-touch events occurring when
	// there's just the last touch left in a multitouch event.
	public static boolean pointerup = false;

	// Force calculator
	public static IForceCalculator forceCalculator;

	public static ParticleSizeIndicator sizeIndicator;

	/**
	 * Creates a game using the properties in the preferences in the context
	 *
	 * @param context
	 */
	public NBodyGame(Context context) {
		super();
		sb = getBeanFromPrefs(PreferenceManager.getDefaultSharedPreferences(context));
		initializeFromBean(sb);
		// Resource textures
		TextureHandler.allocateTextures(R.drawable.star, R.drawable.antistar, R.drawable.staticstar, R.drawable.star2,
				R.drawable.antistar2, R.drawable.staticstar2, R.drawable.star3, R.drawable.antistar3,
				R.drawable.staticstar3, R.drawable.bh, R.drawable.bh2, R.drawable.cross);
		// No resource textures
		TextureHandler.allocateTexture(Background.BG_TEXTURE_ID, false);
		// Initialize sound manager
		if (sound)
			SoundManager.init(context);
	}

	/**
	 * Creates a game using the given simulation bean
	 *
	 * @param context
	 * @param sb
	 */
	public NBodyGame(Context context, SimulationBean sb) {
		super();
		initializeFromBean(sb);
		// Resource textures
		TextureHandler.allocateTextures(R.drawable.star, R.drawable.antistar, R.drawable.staticstar, R.drawable.bh,
				R.drawable.bh2, R.drawable.cross);
		// No resource textures
		TextureHandler.allocateTexture(Background.BG_TEXTURE_ID, false);
		// Initialize sound manager
		if (sound)
			SoundManager.init(context);
	}

	/**
	 * Initializes the system
	 */
	public void init(SimulationBean sb) {

		r = Math.min(w, h) / 2;
		// Slightly larger than screen itself
		if (simAreaScreen) {
			boundaryW = w;
			boundaryH = h;
		} else {
			boundaryW = boundaryBigSide;
			boundaryH = boundaryBigSide;
		}
		boundary = new Rectangle(-boundaryW / 2, -boundaryH / 2, boundaryW, boundaryH);

		// Create force calculator depending on selected method - Mesh
		// resolution will depend on simulation area
		forceCalculator = forceMethod == ForceMethod.DIRECT ? new ForceDirect() : (simAreaScreen ? new ForcePM(
				meshDensity) : new ForcePM(meshDensity + 3));

		// Init vector and polygon pool
		// vectors in pool used in bodies and walls, each having 4 vectors, and
		// in sparks, having 1 vector
		// plus 10 additional vectors for performing operations
		VectorPool.initVectorPool((maxBodies + maxWalls) * 4 + maxSparks + forceCalculator.getVectorsUsed() + 100);
		PolygonPool.initPolygonPool(maxSparks);

		// Initialize force calculator, note it uses the vector pool
		forceCalculator.initialize(boundary.getX(), boundary.getY(), boundary.getWidth(), boundary.getHeight(),
				displayGrid);

		sparks = new FixedSizeArray<Sparks>(maxSparks);
		bodies = new FixedSizeArray<Body>(sb == null ? maxBodies : Math.max(sb.particles.size(), maxBodies));
		walls = new FixedSizeArray<Wall>(maxWalls);

		// initialize static vectors
		un = new Vector2();
		ut = new Vector2();
		aux = new Vector2();
		repPos = new Vector2();
		centre = new Vector2();
		fvec = new Vector2();

		initBodiesAndWalls(sb);

		// Initialize background, draw it centered in screen
		background = new Background(new Vector2(0, 0));

		// Initialize charge and wall linges. We don't use the pool in these
		charge = new EventMotion(new Vector2(0f, 0f), new Vector2(0f, 0f), .5f, .5f, .1f, 1f);
		wall = new EventMotion(new Vector2(0f, 0f), new Vector2(0f, 0f), .54f, .352f, .168f, 1f);

		// Initialize the size indicator
		sizeIndicator = new ParticleSizeIndicator(w, h);

		initCamera();

		// Force GC
		System.gc();
		initialized = true;
	}

	private void initBodiesAndWalls(SimulationBean sb) {
		// Synchronize with body render loop
		synchronized (Locks.l.renderLockBody) {
			if (sb == null) {
				// Create central mass
				if (bh) {
					blackHole = new Body(VectorPool.pool.allocate(centre.x, centre.y), VectorPool.pool.allocate(),
							8E11f, ColorHelper.ch.getBHColor(), StarType.BLACKHOLE);
					bodies.add(blackHole);
				}
				for (int i = 0; i < N; i++) {
					/*
					 * Initialize bodies: x - random between 0 and WIDTH y -
					 * random between 0 and HEIGHT fx - random between -1N and
					 * 1N fy - random between -1N and 1N mass - 1.E8
					 */
					// Force circular distribution
					Vector2 pos = VectorPool.pool.allocate();
					setNewRandomPosition(pos);
					Vector2 vel = VectorPool.pool.allocate();
					setRotationForce(vel, pos, centre, rotForceModulus, r);
					bodies.add(new Body(pos, vel, particleMass, ColorHelper.ch.generateColor()));

				}
			} else {
				// ADD BODIES
				// First, the BH
				for (ParticleBean p : sb.particles) {
					if (p.bh) {
						float x = between(p.x, 0, w) ? p.x : w - 1;
						float y = between(p.y, 0, h) ? p.y : h - 1;
						blackHole = new Body(VectorPool.pool.allocate(x, y), VectorPool.pool.allocate(), p.mass,
								p.color, StarType.BLACKHOLE);
						bodies.add(blackHole);
					}
				}
				// Now, the rest
				for (ParticleBean p : sb.particles) {
					Vector2 pos = VectorPool.pool.allocate();
					pos.set(p.x, p.y);
					Vector2 vel = VectorPool.pool.allocate();
					vel.set(p.vx, p.vy);
					bodies.add(new Body(pos, vel, p.mass, p.radius, p.color, StarType.valueOf(p.starType)));
				}

				// ADD WALLS
				for (WallBean w : sb.walls) {
					Vector2 ini = VectorPool.pool.allocate();
					ini.set(w.x1, w.y1);
					Vector2 end = VectorPool.pool.allocate();
					end.set(w.x2, w.y2);
					walls.add(new Wall(ini, end));
				}
			}
		}
	}

	public void initCamera() {
		// Work out zoom taking as a reference w=400 zoom=1
		// zoom = (float) w / 400;
		zoom = 1f;
		camera = new Camera(0f, 0f);
	}

	/**
	 * Updates the game state given the time increment dt in milliseconds
	 *
	 * @param dt
	 */
	public synchronized void update(long dt) {
		try {
			// Max dt = 100
			// if (dt > 100)
			// dt = 100;

			float dt2 = dt * dtscale;
			// // </DoNotRemove>

			/**
			 * FORCE CALCULATOR
			 **/
			forceCalculator.calculateForces(bodies);

			// This to prevent halt when there's only one body left
			if (bodies.mCount == 1)
				bodies.get(0).treated = false;

			/**
			 * INTEGRATOR
			 **/
			Body bi, bj;
			Body moveToFront = null;
			synchronized (Locks.l.renderLockBody) {
				// Update positions

				for (int i = 0; i < bodies.mCount; i++) {
					bi = bodies.get(i);
					if (!bi.treated) {
						if (bi.isMovable()) {
							// Calculate acceleration vector - a=f/m
							Vector2 acc = bi.force.scale(1f / bi.mass);

							if (accelerometer) {
								acc.x -= gx / 600f;
								acc.y += gy / 600f;
							}
							// Work out new body velocity using acceleration -
							// dv=a*dt
							bi.vel.add(acc.scale(dt2));
							bi.vellength = bi.vel.length();
							// Check max velocity
							if (maxVel > 0 && bi.vellength > maxVel)
								bi.vel.normalize().scale(maxVel);
						}
						/**
						 * COMPUTE COLLISIONS
						 */
						if (bi.hasCollision() && bi.starType != StarType.BLACKHOLE) {
							// Collision can happen between movable and static
							// particles. Not with black holes.
							switch (collisions) {
								case ELASTIC:
									// No collisions with black holes
									while ((bj = bi.nextCollide()) != null && bj.starType != StarType.BLACKHOLE) {
										// Compute elastic collisions
										float bivel = bi.vellength;
										float bjvel = bj.vellength;

										// First, allocate ut and un from pool
										un.set(bj.pos).sub(bi.pos).normalize();
										ut.set(-un.y, un.x);

										// Normal and tangential velocities before
										// collision
										float v1n = bi.vel.dot(un);
										float v1t = bi.vel.dot(ut);

										float v2n = bj.vel.dot(un);
										float v2t = bj.vel.dot(ut);

										// Normal velocities after collision,
										// tangential do not change
										// Uncomment to allow different mass
										// collisions
										float v1nfs = (v1n * (bi.mass - bj.mass) + 2 * bj.mass * v2n)
												/ (bi.mass + bj.mass);
										float v2nfs = (v2n * (bj.mass - bi.mass) + 2 * bi.mass * v1n)
												/ (bj.mass + bi.mass);

										// Vector2 v1nf = un.copy().multiply(
										// /* v1nfs */v2n);
										// Vector2 v1tf = ut.copy().multiply(v1t);
										//
										// Vector2 v2nf = un.copy().multiply(
										// /* v2nfs */v1n);
										// Vector2 v2tf = ut.copy().multiply(v2t);

										if (bi.isMovable() && bj.isMovable()) {
											bi.vel.set(un);
											bj.vel.set(un);

											aux.set(ut);
											bi.vel.multiply(v1nfs /* v2n */).add(aux.multiply(v1t));

											aux.set(ut);
											bj.vel.multiply(v2nfs /* v1n */).add(aux.multiply(v2t));
										} else if (bi.isMovable() && !bj.isMovable()) {
											bi.vel.set(un);
											aux.set(ut);
											bi.vel.multiply(v1nfs /* v2n */).add(aux.multiply(v1t)).normalize()
													.multiply(bivel);
										} else if (bj.isMovable() && !bi.isMovable()) {
											bj.vel.set(un);
											aux.set(ut);
											bj.vel.multiply(v2nfs /* v1n */).add(aux.multiply(v2t)).normalize()
													.multiply(bjvel);
										}
										// Update positions
										float dist = bi.distance(bj);
										if (dist <= bi.getEffectiveRadius() + bj.getEffectiveRadius()) {
											float diff = bi.getEffectiveRadius() + bj.getEffectiveRadius() - dist + .2f;
											if ((bi.isMovable() && bj.isMovable())
													|| (!bi.isMovable() && !bj.isMovable())) {
												// If both or none are movable, we
												// separate them using half of the
												// distance between their centers
												aux.set(un).normalize().scale(diff / 2f);
												bi.pos.sub(aux);
												bj.pos.add(aux);
											} else if (bi.isMovable() && !bj.isMovable()) {
												// Only bi is movable, so all the
												// movement goes to bi!
												aux.set(un).normalize().scale(diff);
												bi.pos.sub(aux);
											} else if (bj.isMovable() && !bi.isMovable()) {
												aux.set(un).normalize().scale(diff);
												bj.pos.add(aux);
											}
										}
										bi.force.zero();
										bj.force.zero();

										bj.treated = true;

										// Add minispark
										if (showSparks && sparks.mCount < maxSparks)
											sparks.add(new Sparks(VectorPool.pool.allocate(un).normalize()
													.multiply(bi.getEffectiveRadius()).add(bi.pos), .2f, -.02f, 15));

										if (sound)
											SoundManager.instance.playRandom();
									}
									break;
								case MERGERS:
									bj = bi.nextCollide();
									// Compute mergers
									if (bi.starType == StarType.BLACKHOLE) {
										bj.makeBlackHole();
										moveToFront = bj;
									} else if (bi.starType == StarType.STATIC) {
										bj.makeStatic();
									}
									float msum = bi.mass + bj.mass;
									float mi = bi.mass / msum;
									float mj = bj.mass / msum;

									if (bi.radius > bj.radius) {
										// Change color
										bj.color = bi.color;
										bj.updateColor();
									}

									// Calculate velocity to add to sparks, maximum
									// of 2 particles
									bj.vel.scale(mj).add(bi.vel.scale(mi));
									if (bi.mass != bj.mass) {
										if (bi.mass > bj.mass) {
											bj.pos.set(bi.pos);
											// Star type of the most massive
											if (bj.isMovable())
												bj.starType = bi.starType;
										}
									} else {
										bj.pos.add(bj.vector(bi).scale(mi));
									}
									if (!bi.isMovable())
										bj.pos.set(bi.pos);

									bj.setMass(msum, bi.radius);

									if (showTrail) {
										// Clear trail and auxiliar buffer
										bj.clearTail();
										// Add new position to trail
										bj.addToTail(bj.pos.x, bj.pos.y);
									}
									// Let's remove bi from the list of bodies
									bi.release();
									bodies.remove(i);
									i--;
									bj.treated = true;
									// Add spark
									if (showSparks && sparks.mCount < maxSparks)
										sparks.add(new Sparks(VectorPool.pool.allocate(bj.pos), 5f, -.01f));
									break;
							}
						}

						/**
						 * PROPER INTEGRATOR - UPDATES PARTICLE POSITIONS USING
						 * CALCULATED VELOCITIES
						 */
						else if (bi.isMovable()) {
							// Update position
							// Apply friction
							float vellength = Math.max(bi.vellength - frictionScalar * dt2, 0f);
							bi.vel.normalize().scale(vellength);
							float newx = bi.pos.x + (bi.vel.x) * dt2;
							float newy = bi.pos.y + (bi.vel.y) * dt2;

							float boundaryWFromCenter = boundary.getWidth() / 2;
							float boundaryHFromCenter = boundary.getHeight() / 2;
							if (bc) {
								float r = bi.getEffectiveRadius();
								// Never let particle out the screen
								if (newx < r - boundaryWFromCenter) {
									bi.updatePosition(r - boundaryWFromCenter, newy);
								} else if (newx > boundaryWFromCenter - r) {
									bi.updatePosition(boundaryWFromCenter - r, newy);
								} else if (newy < r - boundaryHFromCenter) {
									bi.updatePosition(newx, r - boundaryHFromCenter);
								} else if (newy > boundaryHFromCenter - r) {
									bi.updatePosition(newx, boundaryHFromCenter - r);
								} else {
									bi.updatePosition(newx, newy);
								}

								// Boundary conditions, bounce back in, mirror
								// velocity vector with some lose
								Vector2 l = null;
								if (bi.pos.x == r - boundaryWFromCenter || bi.pos.x == boundaryWFromCenter - r)
									l = new Vector2(1, 0);
								if (bi.pos.y == r - boundaryHFromCenter || bi.pos.y == boundaryHFromCenter - r)
									l = new Vector2(0, 1);
								if (l != null) {
									getMirrorVector(l, bi.vel, bi.vellength);
								}
							} else {
								bi.updatePosition(newx, newy);

								if (!boundary.contains(bi.pos.x, bi.pos.y))
									if (!restore) {
										// Delete particle if out of bounds and
										// not restore
										bi.release();
										bodies.remove(i);
									} else {
										// Generate new position and velocity
										// for the particle
										setNewRandomPosition(bi.pos);
										setRotationForce(bi.vel, bi.pos, centre, rotForceModulus, r);
										bi.clearTail();
									}
							}
							bi.treated = true;
						}
						// Angle
						bi.angle += .8f * dt2;
						if (bi.angle > 360)
							bi.angle = 0f;
					}

				}

				if (showSparks) {
					for (int i = 0; i < sparks.mCount; i++) {
						Sparks spark = sparks.get(i);
						spark.update(dt2);
						if (spark.finished()) {
							spark.release();
							sparks.remove(i);
						}
					}
				}
			}

			sizeIndicator.update(dt);

			// Move to front so that black hole is always painted at the
			// background
			if (moveToFront != null) {
				bodies.sort(true);
			}
		} catch (Exception e) {
			Log.e("NBodyGame", e + " : " + e.getMessage());
		}

	}

	/**
	 * Assigns a new force value to the vector in given the current particle
	 * position and the centre of the screen
	 *
	 * @param in
	 * @param position
	 * @param centre
	 * @param modulus
	 * @param radius
	 * @return
	 */
	private static void setRotationForce(Vector2 in, Vector2 position, Vector2 centre, float modulus, float radius) {
		in.set(centre).sub(position).makePerpendicular().normalize().scale(modulus);
	}

	/**
	 * Mirrors the vector v against the line defined by l.
	 *
	 * @param l
	 * @param v
	 *            , as reference
	 * @return
	 */
	public static void getMirrorVector(Vector2 l, Vector2 v, float modulus) {
		aux.set(l);
		v.set(aux.scale(2 * (v.dot(l) / l.dot(l))).sub(v).normalize().scale(-modulus));
	}

	public static int getW() {
		return w;
	}

	public static void setW(int w) {
		NBodyGame.w = w;
	}

	public static int getH() {
		return h;
	}

	public static void setH(int h) {
		NBodyGame.h = h;
	}

	public void addParticle(Vector2 pos, Vector2 vel, StarType type, float mass) {
		bodies.add(new Body(pos, vel, mass, ColorHelper.ch.generateColor(), type));
	}

	public void addParticle(Vector2 pos, Vector2 vel, StarType type) {
		addParticle(pos, vel, type, particleMass);
	}

	public void setNewRandomPosition(Vector2 in) {
		in.set(Constants.rng.nextInt(w) - w / 2, Constants.rng.nextInt(h) - h / 2);
	}

	public void onSensorChanged(SensorEvent event) {
		if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER && accelerometer) {
			// Update values
			gx = event.values[0];
			gy = event.values[1];
		}
	}

	public void onTouch(MotionEvent event, TouchState state) {

		if (event.getPointerCount() > 1 && !simAreaScreen) {
			// Zoom, multitouch, only in big area mode
			switch (event.getAction()) {
				case MotionEvent.ACTION_POINTER_2_DOWN:
					oldDist = spacing(event);
					if (oldDist > 10f) {
						currentEvent = Event.ZOOM;
						Log.d(Constants.LOG_TOUCH, "Multitouch down - Dist: " + oldDist);
					}

					break;

				case MotionEvent.ACTION_MOVE:
					if (currentEvent == Event.ZOOM) {
						float dist = spacing(event);
						if (dist > 10f) {
							float zoomScale = dist / oldDist;
							zoom *= zoomScale;
							if (zoom > maxScale)
								zoom = maxScale;
							else if (zoom < minScale)
								zoom = minScale;

							oldDist = dist;
							Log.d(Constants.LOG_TOUCH, "New Dist: " + oldDist);
						}
					}
					break;

				case MotionEvent.ACTION_POINTER_1_UP:
					if (currentEvent == Event.ZOOM) {
						currentEvent = Event.NONE;
					}
					pointerup = true;
					break;
				case MotionEvent.ACTION_POINTER_2_UP:
					if (currentEvent == Event.ZOOM) {
						currentEvent = Event.NONE;
					}
					pointerup = true;
					break;
			}
		} else if (pointerup) {
			// We need to wait for the second touch of a multitouch event to
			// finish
			if (event.getAction() == MotionEvent.ACTION_UP)
				pointerup = false;
		} else {
			// Single touch

			// Current pan vector
			Vector2 pan = camera.getPan();
			// Convert from screen reference system [(0,0), (w,h)]
			// to scene reference system [(-w/2,-h/2), (w/2,h/2)], centered at
			// (0,0)
			float x = event.getX() - w / 2;
			float y = event.getY() - h / 2;

			if (state == TouchState.SHOOT || state == TouchState.SHOOT_ANTI || state == TouchState.SHOOT_STATIC) {
				x = x / zoom;
				y = y / zoom;
				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					// Initialize touch sequence event
					charge.setTini(x, y);
					charge.setTcurrent(x, y);
					currentEvent = Event.SHOOT;
				} else if (event.getAction() == MotionEvent.ACTION_MOVE) {
					charge.setTcurrent(x, y);
				} else if (event.getAction() == MotionEvent.ACTION_UP) {
					charge.setTcurrent(x, y);
					// Add particle if we still have space
					if (bodies.mCount < maxBodies) {
						Vector2 vel = VectorPool.pool.allocate(charge.getTini()).sub(charge.getTcurrent()).scale(0.03f);
						if (vel.length() > vmax)
							vel.normalize().scale(vmax);
						Vector2 pos = VectorPool.pool.allocate(charge.getTcurrent()).add(pan.x, pan.y);
						// Add particle synchronized with render and update
						synchronized (Locks.l.renderLockBody) {
							StarType st = getStarType(state);
							addParticle(pos, st == StarType.STATIC ? vel.scale(0f) : vel, st,
									sizeIndicator.getMassValue());
						}
						charge.reset();
					} else {
						Log.d("NBodyGame", "Maximum number of bodies reached");
					}
					currentEvent = Event.NONE;
				}
			} else if (state == TouchState.REPEL) {
				// Implement repulsion
				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					currentEvent = Event.REPEL;
					repPos.set(x, y).add(pan);
				} else if (event.getAction() == MotionEvent.ACTION_MOVE) {
					repPos.set(x, y).add(pan);
				} else if (event.getAction() == MotionEvent.ACTION_UP) {
					currentEvent = Event.NONE;
				}
			} else if (state == TouchState.WALL) {
				x = x / zoom;
				y = y / zoom;
				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					// Initialize touch sequence event
					wall.setTini(x, y);
					wall.setTcurrent(x, y);
					currentEvent = Event.WALL;
				} else if (event.getAction() == MotionEvent.ACTION_MOVE) {
					wall.setTcurrent(x, y);
				} else if (event.getAction() == MotionEvent.ACTION_UP) {
					wall.setTcurrent(x, y);
					// Add wall to game
					if (walls.mCount < maxWalls - 1)
						walls.add(new Wall(VectorPool.pool.allocate(wall.getTcurrent().add(pan)), VectorPool.pool
								.allocate(wall.getTini().add(pan))));

					// Release vectors
					wall.reset();
					currentEvent = Event.NONE;
				}
			} else if (state == TouchState.PAN && !simAreaScreen) {
				// Pan, only in big area mode
				x = x / zoom;
				y = y / zoom;

				panBoundX = boundary.getWidth() / 2;
				panBoundY = boundary.getHeight() / 2;

				// Implement repulsion
				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					// Initialize touch sequence event
					camera.setTini(x, y);
					camera.panning = true;
					currentEvent = Event.PAN;
				} else if (event.getAction() == MotionEvent.ACTION_MOVE) {
					Vector2 targetPan = camera.getPan(x, y);
					// Check boundary overflow
					if (Math.abs(targetPan.x) > panBoundX) {
						camera.difference.x = Math.signum(targetPan.x) * panBoundX;
						x = camera.pos.x;
					}
					if (Math.abs(targetPan.y) > panBoundY) {
						camera.difference.y = Math.signum(targetPan.y) * panBoundY;
						y = camera.pos.y;
					}

					camera.setTcurrent(x, y);
					camera.updateCurrentDifference();
				} else if (event.getAction() == MotionEvent.ACTION_UP) {
					camera.updateDifference();
					camera.clearCurrentDifference();
					camera.panning = false;
					currentEvent = Event.NONE;
				}

			}
		}
	}

	private static float spacing(MotionEvent event) {
		float x = event.getX(0) - event.getX(1);
		float y = event.getY(0) - event.getY(1);
		return (float) Math.sqrt(x * x + y * y);
	}

	private boolean between(Number num, Number a, Number b) {
		return num.doubleValue() >= a.doubleValue() && num.doubleValue() <= b.doubleValue();
	}

	private boolean gr(Number num, Number a) {
		return num.doubleValue() > a.doubleValue();
	}

	private boolean geq(Number num, Number a) {
		return num.doubleValue() >= a.doubleValue();
	}

	private void initializeFromBean(SimulationBean sb) {
		N = between(sb.N, 0, maxBodies) ? sb.N : maxBodies;
		dtscale = between(sb.dtscale, 0f, 100f) ? sb.dtscale : 0;
		bc = sb.bc;
		collisions = CollisionPolicy.valueOf(sb.collisions);
		ColorHelper.initialize(sb.colorPolicy);
		showSparks = sb.showSparks;
		gstrength = geq(sb.gstrength, 0) ? sb.gstrength : 0;
		accelerometer = sb.accelerometer;
		restore = sb.restore;
		bgRed = (float) Color.red(sb.bgColor) / 255f;
		bgGreen = (float) Color.green(sb.bgColor) / 255f;
		bgBlue = (float) Color.blue(sb.bgColor) / 255f;
		bgAlpha = (float) Color.alpha(sb.bgColor) / 255f;
		simAreaScreen = sb.simAreaScreen;

		bhSprite = sb.bhSprite;
		bh = sb.bh;

		starSprite = sb.starSprite;

		forceMethod = ForceMethod.valueOf(sb.forceMethod);
		displayGrid = sb.displayGrid;
		displayMeshPoints = sb.displayMeshPoints;
		meshDensity = sb.meshDensity;

		// Background image
		bgImage = sb.bgImage;

		showTrail = sb.showTail;
		ntail = geq(sb.ntail, 0) ? sb.ntail : 60;
		particleMass = gr(sb.particleMass, 0) ? sb.particleMass : Body.minMass;
		friction = Friction.valueOf(sb.friction);
		setFriction(friction);
	}

	private SimulationBean getBeanFromPrefs(SharedPreferences sp) {
		assert sp != null : "Preferences are null!";
		SimulationBean sb = new SimulationBean();

		sb.N = sp.getInt(Constants.PREF_PARTICLECOUNT, 10);
		sb.dtscale = computeDtscale(sp.getInt(Keys.KEY_VELOCITY, Constants.DEFAULT_VELOCITY));
		sb.bc = sp.getBoolean(Constants.PREF_BOUNDARY, true);
		sb.collisions = sp.getString(Constants.PREF_COLLISIONS, CollisionPolicy.NONE.toString()).toUpperCase();
		sb.colorPolicy = Integer.parseInt(sp.getString(Constants.PREF_PARTICLECOLOR, "4"));
		sb.showSparks = sp.getBoolean(Constants.PREF_SHOWSPARKS, true);
		sb.gstrength = Float.parseFloat(sp.getString(Constants.PREF_GSTRENGTH, "1.2"));

		// Accelerometer not controlled from preferences, but from controls in
		// screen
		sb.accelerometer = accelerometer;

		sb.restore = sp.getBoolean(Constants.PREF_RESTOREPARTICLES, true);
		sb.bgColor = sp.getInt(Constants.PREF_BGCOLOR, Color.BLACK);
		sb.simAreaScreen = sp.getString(Constants.PREF_SIMAREA, Constants.SIMAREA_SCREEN).equals(
				Constants.SIMAREA_SCREEN);

		sb.bhSprite = sp.getInt(Constants.PREF_CHOOSEBH, 0);
		// There is black hole if value is 0 or 1
		sb.bh = sb.bhSprite != 2;

		sb.starSprite = sp.getInt(Constants.PREF_CHOOSESTAR, 0);

		sb.forceMethod = sp.getString(Constants.PREF_FORCEMETHOD, ForceMethod.DIRECT.toString());
		sb.displayGrid = sp.getBoolean(Constants.PREF_DISPLAYGRID, true);
		sb.displayMeshPoints = sp.getBoolean(Constants.PREF_DISPLAYMESHPOINTS, true);
		sb.meshDensity = sp.getInt(Constants.PREF_MESHDENSITY, 2) + 3;

		// Background image
		sb.bgImage = sp.getString(Constants.PREF_BGIMAGE, null);

		// ShowTail not controlled from preferences, but from controls in screen
		sb.showTail = showTrail;
		// Tail length

		sb.ntail = Integer.parseInt(sp.getString(Constants.PREF_TAILLENGTH, "14"));
		if (sb.ntail != 14 && sb.ntail != 24 && sb.ntail != 34)
			sb.ntail = 14;

		// Initial particle mass
		sb.particleMass = Float.parseFloat(sp.getString(Constants.PREF_PARTICLEMASS, "8E9"));

		// Friction
		sb.friction = sp.getString(Constants.PREF_FRICTION, "no").toUpperCase();

		return sb;
	}

	private void setFriction(Friction friction) {
		switch (friction) {
			case NONE:
			case NO:
				frictionScalar = 0f;
				break;
			case SMALL:
				frictionScalar = 2E-3f;
				break;
			case MEDIUM:
				frictionScalar = 5E-3f;
				break;
			case LARGE:
				frictionScalar = 1E-2f;
				break;
			case EXTRALARGE:
				frictionScalar = 3.3E-2f;
				break;
		}
	}

	/**
	 * Simple interpolation from .05 to 1
	 *
	 * @param sliderValue
	 * @return
	 */
	public static float computeDtscale(int sliderValue) {
		LinearInterpolator li = new LinearInterpolator(new float[] { 0.f, 100.f }, new float[] { .0f, 1.0f });
		return li.value(sliderValue);
	}

	public static int decodeDtscale(float dtscale) {
		LinearInterpolator li = new LinearInterpolator(new float[] { 0.f, 1.0f }, new float[] { .0f, 100f });
		return (int) li.value(dtscale);
	}

	private StarType getStarType(TouchState state) {
		if (state == TouchState.SHOOT_ANTI)
			return StarType.ANTISTAR;
		else if (state == TouchState.SHOOT_STATIC)
			return StarType.STATIC;
		return StarType.STAR;
	}

	/**
	 * Exports the current game to a simulation bean
	 *
	 * @return
	 */
	public SimulationBean exportToSimulationBean() {

		sb.particles.clear();
		sb.walls.clear();

		// Add bodies
		for (int i = 0; i < bodies.mCount; i++) {
			Body b = bodies.get(i);
			sb.particles.add(b.exportToBean());
		}

		for (int i = 0; i < walls.mCount; i++) {
			Wall w = walls.get(i);
			sb.walls.add(w.exportToBean());
		}

		return sb;
	}

	public void setShowTail(boolean showTail) {
		showTrail = showTail;
		if (sb != null)
			sb.showTail = showTail;
	}

	public void setAccelerometer(boolean accelerometer) {
		NBodyGame.accelerometer = accelerometer;
		if (sb != null)
			sb.accelerometer = accelerometer;
	}

	public void setDtscale(float dtscale) {
		this.dtscale = dtscale;
		if (sb != null)
			sb.dtscale = dtscale;
	}

}
