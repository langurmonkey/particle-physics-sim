package com.nbody.core.xml;

/**
 * Basic XML parser implementation.
 * 
 * @author Toni Sagrista
 * 
 */
public abstract class BaseParser implements IParser {

	// names of the XML tags
	static final String TAG_SIMULATION = "simulation";
	static final String TAG_PARTICLES = "particles";
	static final String TAG_PARTICLE = "particle";
	static final String TAG_WALLS = "walls";
	static final String TAG_WALL = "wall";

}
