package com.nbody.core.graph;

import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLSurfaceView.Renderer;
import android.util.Log;

import com.nbody.core.game.Background;
import com.nbody.core.game.Body;
import com.nbody.core.game.NBodyGame;
import com.nbody.core.game.NBodyGame.Event;
import com.nbody.core.game.NBodyGame.ForceMethod;
import com.nbody.core.game.Sparks;
import com.nbody.core.game.Wall;
import com.nbody.core.geom.Vector2;
import com.nbody.core.util.Locks;

/**
 * That's the renderer extension for the simulator. Is in charge of invoking the render methods on all the elements in
 * the scene.
 * 
 * @author Toni Sagrista
 */
public class GameRenderer implements Renderer {
	private long sumdt, fcount, lastT;
	public float fps;
	public boolean showFps = true;

	private int mWidth;
	private int mHeight;

	private Square bounds;

	/** The Activity Context ( NEW ) */
	private Context mContext;
	// Specifies the format our textures should be converted to upon load.
	private static BitmapFactory.Options sBitmapOptions = new BitmapFactory.Options();
	// An array of things to draw every frame.
	// Pre-allocated arrays to use at runtime so that allocation during the
	// test can be avoided.
	private int[] mTextureNameWorkspace;
	private int[] mCropWorkspace;
	// A reference to the application context.
	// Determines the use of vertex arrays.
	private boolean mUseVerts;
	// Determines the use of vertex buffer objects.
	private boolean mUseHardwareBuffers;

	private boolean touchCamera = false;

	// Scale factor of width
	private float scaleFactor;

	float angle = 0;

	/**
	 * Instance the Cube object and set the Activity Context handed over
	 */
	public GameRenderer(Context context, int gameWidth, int gameHeight) {
		this.mContext = context;
		// Pre-allocate and store these objects so we can use them at runtime
		// without allocating memory mid-frame.
		mTextureNameWorkspace = new int[1];
		mCropWorkspace = new int[4];

		// Set our bitmaps to 16-bit, 565 format.
		sBitmapOptions.inPreferredConfig = Bitmap.Config.RGB_565;
		mWidth = gameWidth;
		mHeight = gameHeight;

		lastT = System.currentTimeMillis();
		sumdt = 0;
		fps = 0;
	}

	public int[] getConfigSpec() {
		// We don't need a depth buffer, and don't care about our
		// color depth.
		int[] configSpec = { EGL10.EGL_DEPTH_SIZE, 0, EGL10.EGL_NONE };
		return configSpec;
	}

	/**
	 * Changes the vertex mode used for drawing.
	 * 
	 * @param useVerts
	 *            Specifies whether to use a vertex array. If false, the
	 *            DrawTexture extension is used.
	 * @param useHardwareBuffers
	 *            Specifies whether to store vertex arrays in main memory or on
	 *            the graphics card. Ignored if useVerts is false.
	 */
	public void setVertMode(boolean useVerts, boolean useHardwareBuffers) {
		mUseVerts = useVerts;
		mUseHardwareBuffers = useVerts && useHardwareBuffers;
	}

	/* Called when the size of the window changes. */
	public void sizeChanged(GL10 gl, int width, int height) {
		gl.glViewport(0, 0, width, height);

		/*
		 * Set our projection matrix. This doesn't have to be done each time we
		 * draw, but usually a new projection needs to be set when the viewport
		 * is resized.
		 */
		gl.glMatrixMode(GL10.GL_PROJECTION);
		gl.glLoadIdentity();
		gl.glOrthof(0.0f, width, 0.0f, height, 0.0f, 1.0f);

		gl.glShadeModel(GL10.GL_FLAT);
		gl.glEnable(GL10.GL_BLEND);
		gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
		gl.glColor4x(0x10000, 0x10000, 0x10000, 0x10000);
		gl.glEnable(GL10.GL_TEXTURE_2D);
	}

	/**
	 * If the surface changes, reset the view
	 */
	public void onSurfaceChanged(GL10 gl, int width, int height) {
		if (height == 0) { // Prevent A Divide By Zero By
			height = 1; // Making Height Equal One
		}

		gl.glViewport(0, 0, width, height); // Reset The Current Viewport
		positionCamera(gl, NBodyGame.zoom, NBodyGame.camera.getPan(), true);

	}

	/**
	 * Called whenever the surface is created. This happens at startup, and may
	 * be called again at runtime if the device context is lost (the screen goes
	 * to sleep, etc). This function must fill the contents of vram with texture
	 * data and (when using VBOs) hardware vertex arrays.
	 */
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig eglc) {
		// Initialize textures
		TextureHandler.loadAll(mContext, gl);
		/*
		 * Some one-time OpenGL initialization can be made here probably based
		 * on features of this particular context
		 */

		// Set the background color to black ( rgba ).
		gl.glClearColor(0.0f, 0.0f, 0.0f, 1f);

		// Enable Smooth Shading, default not really needed.
		gl.glShadeModel(GL10.GL_SMOOTH);
		// Depth buffer setup.
		gl.glClearDepthf(1.0f);
		// Enables depth testing.
		gl.glEnable(GL10.GL_DEPTH_TEST);
		// The type of depth testing to do.
		gl.glDepthFunc(GL10.GL_LEQUAL);
		// Really nice perspective calculations.

		gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA); // enable
		// transparency
		gl.glEnable(GL10.GL_BLEND); // enable transparency blending

		// gl.glHint(GL10.GL_PERSPECTIVE_CORRECTION_HINT, GL10.GL_NICEST);
		gl.glHint(GL10.GL_POLYGON_SMOOTH_HINT, GL10.GL_NICEST);

		// Initialize bounds
		bounds = new Square(NBodyGame.boundary.getHeight() / NBodyGame.boundary.getWidth());
		scaleFactor = NBodyGame.boundary.getWidth();

		// Init bg image
		String bgImage = NBodyGame.bgImage;
		if (bgImage != null && bgImage.length() > 0) {
			TextureHandler.loadTexture(mContext, gl, bgImage, Background.BG_TEXTURE_ID);
			NBodyGame.background.display = true;
			Log.i("GameRenderer", "Background texture loaded: " + bgImage);
		} else {
			NBodyGame.background.display = false;
			Log.i("GameRenderer", "Not using background");
		}
	}

	/**
	 * Called when the rendering thread shuts down. This is a good place to
	 * release OpenGL ES resources.
	 * 
	 * @param gl
	 */
	public void shutdown(GL10 gl) {

	}

	public synchronized void positionCamera(GL10 gl, float zoom, Vector2 camera, boolean updateCamera) {
		if (touchCamera) {
			updateCamera = true;
			touchCamera = false;
		}
		if (updateCamera) {
			gl.glMatrixMode(GL10.GL_PROJECTION);
			gl.glLoadIdentity();
			zoom = 1 / zoom;

			// Zoom with camera
			gl.glOrthof(-mWidth * zoom / 2, mWidth * zoom / 2, mHeight * zoom / 2, -mHeight * zoom / 2, 0.0f, 1.0f);

		}
		gl.glMatrixMode(GL10.GL_MODELVIEW);
		gl.glLoadIdentity();

		// Pan
		gl.glTranslatef(-camera.x, -camera.y, 0f);
	}

	public synchronized void positionStandardCamera(GL10 gl) {
		gl.glMatrixMode(GL10.GL_PROJECTION);
		gl.glLoadIdentity();
		gl.glOrthof(-mWidth / 2, mWidth / 2, mHeight / 2, -mHeight / 2, 0.0f, 1.0f);

		gl.glMatrixMode(GL10.GL_MODELVIEW);
		gl.glLoadIdentity();
	}

	@Override
	public synchronized void onDrawFrame(GL10 gl) {
		if (showFps)
			updateFps();

		NBodyGame game = NBodyGame.singleton();

		Vector2 camera = NBodyGame.camera.getPan();
		float scale = NBodyGame.zoom;

		// Camera location (zoom and pan)
		positionCamera(gl, scale, camera, NBodyGame.currentEvent == Event.ZOOM || NBodyGame.currentEvent == Event.PAN);

		gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);
		gl.glClearColor(NBodyGame.bgRed, NBodyGame.bgGreen, NBodyGame.bgBlue, NBodyGame.bgAlpha);

		gl.glPushMatrix();
		// First, render boundary square
		gl.glScalef(scaleFactor, scaleFactor, 1f);
		gl.glColor4f(1f, 1f, 1f, 1f);
		gl.glLineWidth(4f * scale);
		bounds.draw(gl);

		gl.glPopMatrix();

		// Then, scene
		synchronized (Locks.l.renderLockBody) {

			// Render force calculator, if any
			if (NBodyGame.displayGrid && NBodyGame.forceCalculator != null)
				NBodyGame.forceCalculator.draw(gl);

			if (mUseVerts) {
				Grid.beginDrawing(gl, true, false);
			} else {
				gl.glDisable(GL10.GL_DEPTH_TEST);
				gl.glEnable(GL10.GL_TEXTURE_2D);
			}

			if (game != null && game.initialized) {
				// Render background
				if (NBodyGame.background != null && NBodyGame.background.display()
						&& !(NBodyGame.forceMethod == ForceMethod.PM && NBodyGame.displayGrid))
					NBodyGame.background.draw(gl);

				// Render bodies
				for (int i = 0; i < game.bodies.mCount; i++) {
					Body body = game.bodies.get(i);
					body.draw(gl);
				}

				// Render mesh points in front of bodies
				if (NBodyGame.displayMeshPoints && NBodyGame.forceCalculator != null)
					NBodyGame.forceCalculator.drawMeshPoints(gl);
			}

			if (mUseVerts) {
				Grid.endDrawing(gl, true, false);
			} else {
				gl.glEnable(GL10.GL_DEPTH_TEST);
				gl.glDisable(GL10.GL_TEXTURE_2D);
			}

			// Draw trails
			if (NBodyGame.showTrail) {
				for (int i = 0; i < game.bodies.mCount; i++) {
					Body body = game.bodies.get(i);
					body.drawTail(gl, 2.5f * scale);
				}
			}

			// Render sparks
			if (NBodyGame.showSparks)
				for (int i = 0; i < game.sparks.mCount; i++) {
					Sparks sparks = game.sparks.get(i);
					sparks.draw(gl);
				}

		}

		// Render walls
		int n = game.walls.mCount;
		gl.glLineWidth(3f * scale);
		for (int i = 0; i < n; i++) {
			Wall wall = game.walls.get(i);
			wall.draw(gl);
		}

		/**
		 * TOUCH EVENTS
		 */
		// Draw shooter line
		if (NBodyGame.currentEvent == Event.SHOOT) {
			Renderable charge = game.charge;
			charge.draw(gl);
		}

		// Draw wall
		if (NBodyGame.currentEvent == Event.WALL) {
			Renderable wall = game.wall;
			wall.draw(gl);
		}

		/**
		 * FIXED ELEMENTS
		 */
		// Render sizeIndicator
		NBodyGame.sizeIndicator.draw(gl, camera, scale);

	}

	public void touchCamera() {
		touchCamera = true;
	}

	private void updateFps() {
		long currentT = System.currentTimeMillis();
		long dt = currentT - lastT;
		lastT = currentT;
		// Compute fps
		if (sumdt >= 1000) {
			fps = fcount * 1000 / sumdt;
			fcount = 0;
			sumdt = 0;
		} else {
			fcount++;
			sumdt += dt;
		}
	}

}
