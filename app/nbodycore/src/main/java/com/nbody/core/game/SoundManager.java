package com.nbody.core.game;

import android.content.Context;

/**
 * A simple sound manager, unused so far.
 * 
 * @author Toni Sagrista
 * 
 */
public class SoundManager {
	public static SoundManager instance;

	private Context mContext;
	private int[] resources;

	public static void init(Context context) {
		if (instance == null)
			instance = new SoundManager(context);
	}

	private SoundManager(Context context) {
		mContext = context;
		resources = new int[4];
		// resources[0] = R.raw.glass1;
		// resources[1] = R.raw.glass2;
		// resources[2] = R.raw.glass1;
		// resources[3] = R.raw.glass4;

	}

	public void playRandom() {
		// MediaPlayer.create(mContext, resources[Constants.rng.nextInt(4)]).start();
	}
}
