package com.nbody.core.game;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11Ext;

import com.nbody.core.geom.Vector2;
import com.nbody.core.graph.GLSprite;
import com.nbody.core.graph.GridHandler;
import com.nbody.core.graph.TextureHandler;

/**
 * Manages the background of the scene.
 * 
 * @author Toni Sagrista
 * 
 */
public class Background extends GLSprite {
	public static final int BG_TEXTURE_ID = 2;
	public boolean display = false;

	public Background(Vector2 pos) {
		super(pos, null);
		this.mTexture = TextureHandler.getTexture(BG_TEXTURE_ID);
		display = false;
	}

	@Override
	public void draw(GL10 gl) {
		// Calculate scale to cover whole screen
		// super.draw(gl, null, NBodyGame.boundaryH / mTexture.mHeight);
		float scale = (float) NBodyGame.boundaryH / (float) mTexture.mHeight;
		// check grid
		if (mGrid == null && mTexture.isLoaded()) {
			// Init grid
			mGrid = GridHandler.getGrid(mTexture.mHeight);
		}

		gl.glBindTexture(GL10.GL_TEXTURE_2D, mTexture.mTextureName);
		if (!GridHandler.useVerts) {
			// Draw using the DrawTexture extension.
			((GL11Ext) gl).glDrawTexfOES(pos.x, pos.y, 0f, mTexture.mWidth, mTexture.mHeight);
		} else {
			// Draw using verts or VBO verts.
			gl.glTexEnvx(GL10.GL_TEXTURE_ENV, GL10.GL_TEXTURE_ENV_MODE, GL10.GL_MODULATE);
			float tx = pos.x - mTexture.mWidth * scale / 2;
			float ty = pos.y - mTexture.mHeight * scale / 2;

			gl.glRotatef(180, 0f, 0f, 1f);
			// gl.glRotatef(180, 0f, 1f, 0f);

			gl.glTranslatef(tx, ty, 0f);
			gl.glScalef(scale, scale, 1f);
			mGrid.draw(gl, true, true);
			gl.glScalef(1 / scale, 1 / scale, 1f);
			gl.glTranslatef(-tx, -ty, 0f);

			// gl.glRotatef(-180, 0f, 1f, 0f);
			gl.glRotatef(-180, 0f, 0f, 1f);
		}
		gl.glClear(GL10.GL_TEXTURE);
	}

	public boolean display() {
		return display && mTexture.isLoaded();
	}

	public boolean isReady() {
		return mTexture.isLoaded();
	}

}
