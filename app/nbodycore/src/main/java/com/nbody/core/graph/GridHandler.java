package com.nbody.core.graph;

import java.util.HashMap;
import java.util.Map;

/**
 * Handles the grids of the simulation.
 * 
 * @author Toni Sagrista
 * 
 */
public class GridHandler {
	public static boolean useVerts = false;
	public static boolean useHardwareBuffers = false;

	public static Map<Integer, Grid> grids = new HashMap<Integer, Grid>();

	public static Grid getGrid(int side) {
		if (!useVerts)
			return null;
		if (grids.containsKey(side))
			return grids.get(side);
		else {
			Grid grid = createGrid(side);
			grids.put(side, grid);
			return grid;
		}

	}

	private static Grid createGrid(int width, int height) {
		// Setup a quad for the sprites to use. All sprites will use the
		// same sprite grid intance.
		Grid grid = new Grid(2, 2, false);
		grid.set(0, 0, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, null);
		grid.set(1, 0, width, 0.0f, 0.0f, 1.0f, 1.0f, null);
		grid.set(0, 1, 0.0f, height, 0.0f, 0.0f, 0.0f, null);
		grid.set(1, 1, width, height, 0.0f, 1.0f, 0.0f, null);
		return grid;
	}

	private static Grid createGrid(int side) {
		return createGrid(side, side);
	}

}
