package com.nbody.core.graph;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11Ext;

import com.nbody.core.geom.Vector2;

/**
 * This is the OpenGL ES version of a sprite. It is more complicated than the
 * CanvasSprite class because it can be used in more than one way. This class
 * can draw using a grid of verts, a grid of verts stored in VBO objects, or
 * using the DrawTexture extension.
 */
public abstract class GLSprite extends Renderable {
	protected Texture mTexture;
	// If drawing with verts or VBO verts, the grid object defining those verts.
	protected Grid mGrid;
	public float angle;

	public GLSprite(Vector2 pos, Vector2 vel) {
		super(pos, vel);
	}

	public void setGrid(Grid grid) {
		mGrid = grid;
	}

	public Grid getGrid() {
		return mGrid;
	}

	public void draw(GL10 gl, float[] color, float scale) {
		// check grid
		if (mGrid == null && mTexture.isLoaded()) {
			// Init grid
			mGrid = GridHandler.getGrid(mTexture.mHeight);
		}

		gl.glBindTexture(GL10.GL_TEXTURE_2D, mTexture.mTextureName);
		if (!GridHandler.useVerts) {
			// Draw using the DrawTexture extension.
			((GL11Ext) gl).glDrawTexfOES(pos.x, pos.y, 0f, mTexture.mWidth, mTexture.mHeight);
		} else {
			// Draw using verts or VBO verts.
			gl.glTexEnvx(GL10.GL_TEXTURE_ENV, GL10.GL_TEXTURE_ENV_MODE, GL10.GL_MODULATE);
			if (color != null)
				gl.glColor4f(color[0], color[1], color[2], color[3]);

			gl.glPushMatrix();

			gl.glTranslatef(pos.x, pos.y, 0f);
			gl.glScalef(scale, scale, 1f);
			gl.glRotatef(angle, 0, 0, 1);
			gl.glTranslatef(-mTexture.mWidth / 2, -mTexture.mHeight / 2, 0f);

			mGrid.draw(gl, true, true);

			gl.glPopMatrix();
		}
		gl.glClear(GL10.GL_TEXTURE);
	}

	public void draw(GL10 gl) {
		draw(gl, null, 1f);
	}
}
