# Particle Physics Simulator

Repository that holds the code and resources of the Particle Physics Simulator android app.
See [this page for more information](https://tonisagrista.com/project/pps/).

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
     alt="Get it on F-Droid"
     height="80">](https://f-droid.org/packages/com.tss.android/)
[<img src="https://play.google.com/intl/en_us/badges/images/generic/en-play-badge.png"
     alt="Get it on Google Play"
     height="80">](https://play.google.com/store/apps/details?id=com.tss.android)

## How to build

Install the Andoid command line tools in `$HOME/android-sdk-linux` and run:

```console
$  export ANDROID_HOME=$HOME/android-sdk-linux
$  yes | $ANDROID_HOME/tools/bin/sdkmanager --sdk_root=${ANDROID_HOME} --licenses
$  $ANDROID_HOME/tools/bin/sdkmanager --sdk_root=${ANDROID_HOME} "platform-tools" "platforms;android-${ANDROID_COMPILE_SDK}" >/dev/null
```

Then, build the project:

```console
$  ./gradlew build
```

And create the signed apk:

```console
$  ./gradlew assembleRelease -Pandroid.injected.signing.store.file=$KEYFILE -Pandroid.injected.signing.store.password=$STORE_PASSWORD -Pandroid.injected.signing.key.alias=$KEY_ALIAS -Pandroid.injected.signing.key.password=$KEY_PASSWORD
```

...or the unsigned apk:

```console
$  ./gradlew assembleRelease
```

