ECLIPSE ANDROID PROJECT IMPORT SUMMARY
======================================

Manifest Merging:
-----------------
Your project uses libraries that provide manifests, and your Eclipse
project did not explicitly turn on manifest merging. In Android Gradle
projects, manifests are always merged (meaning that contents from your
libraries' manifests will be merged into the app manifest. If you had
manually copied contents from library manifests into your app manifest
you may need to remove these for the app to build correctly.

Ignored Files:
--------------
The following files were *not* copied into the new Gradle project; you
should evaluate whether these are still needed in your project and if
so manually move them:

From android-fileexplorer:
* .gitignore
* .gradle/
* .gradle/5.2.1/
* .gradle/5.2.1/executionHistory/
* .gradle/5.2.1/executionHistory/executionHistory.bin
* .gradle/5.2.1/executionHistory/executionHistory.lock
* .gradle/5.2.1/fileChanges/
* .gradle/5.2.1/fileChanges/last-build.bin
* .gradle/5.2.1/fileHashes/
* .gradle/5.2.1/fileHashes/fileHashes.bin
* .gradle/5.2.1/fileHashes/fileHashes.lock
* .gradle/5.2.1/gc.properties
* .gradle/buildOutputCleanup/
* .gradle/buildOutputCleanup/buildOutputCleanup.lock
* .gradle/buildOutputCleanup/cache.properties
* .gradle/buildOutputCleanup/outputFiles.bin
* .gradle/vcs-1/
* .gradle/vcs-1/gc.properties
* Android-fileexplorer.iml
* gradle/
* gradle/wrapper/
* gradle/wrapper/gradle-wrapper.jar
* gradle/wrapper/gradle-wrapper.properties
* gradlew
* gradlew.bat
* proguard.cfg
From nbody-android:
* .gitignore
* NBody-Android.iml
* default.properties
From nbody-core:
* .gitignore
* NBodyCore.iml
* default.properties
* proguard.cfg

Moved Files:
------------
Android Gradle projects use a different directory structure than ADT
Eclipse projects. Here's how the projects were restructured:

In android-fileexplorer:
* AndroidManifest.xml => androidfileexplorer/src/main/AndroidManifest.xml
* res/ => androidfileexplorer/src/main/res/
* src/ => androidfileexplorer/src/main/java/
In nbody-core:
* AndroidManifest.xml => nbodycore/src/main/AndroidManifest.xml
* res/ => nbodycore/src/main/res/
* src/ => nbodycore/src/main/java/
In nbody-android:
* AndroidManifest.xml => nbodyandroid/src/main/AndroidManifest.xml
* res/ => nbodyandroid/src/main/res/
* src/ => nbodyandroid/src/main/java/

Next Steps:
-----------
You can now build the project. The Gradle project needs network
connectivity to download dependencies.

Bugs:
-----
If for some reason your project does not build, and you determine that
it is due to a bug or limitation of the Eclipse to Gradle importer,
please file a bug at http://b.android.com with category
Component-Tools.

(This import summary is for your information only, and can be deleted
after import once you are satisfied with the results.)
